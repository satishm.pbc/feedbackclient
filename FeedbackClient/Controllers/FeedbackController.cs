﻿using ClientFeedback.DAL;
using ClientFeedback.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Net.Mail;
using System.IO;

namespace ClientFeedback.Controllers
{
    public class FeedbackController : ApiController
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Client_Feedback"].ConnectionString);

        Response res = new Response();
        ErrorLog Error = new ErrorLog();
        Feedbackdal Feedbacks = new Feedbackdal();

        [HttpPost]
        public IHttpActionResult InsertFeedback(Feedback IN)
        {
            res.Code = 401;
            res.Data = "";
            res.Message = "Unable to save data";
            try
            {

                string result = Feedbacks.InsertFeedback(IN);
                if (result == "1")
                {
                    SendEnquiryMailToHelpdesk(IN.Name, IN.EmailID, IN.Designation, IN.ClientName);
                    SendEnquiryMailToUser(IN.Name, IN.EmailID);
                    res.Code = 200;
                    res.Data = "";
                    res.Message = "Feedback Submitted Successfully....";
                   

                }
               
                string Req = JsonConvert.SerializeObject(IN);
                string Res = JsonConvert.SerializeObject(res);
                string errorLogged = Error.ReqRes("Feedback", "InsertFeedback", Req, Res);
                return Json(res);

            }
            catch (Exception ex)
            {

                res.Code = 500;
                res.Data = "";
                res.Message = ex.Message;
                int errorLogged = Error.ErrorLogs("Feedback", "InsertFeedback", ex.Message);
                return Json(res);

            }
        }

        [HttpGet]
        public IHttpActionResult GetAllClient()
        {
            res.Code = 401;
            res.Data = "";
            res.Message = "Unable to save data";
            try
            {
            

                DataSet ds = Feedbacks.GetAllClient();
                List<Client> ClientList = new List<Client>();
               
                if (ds.Tables[0].Rows.Count > 0)
                    ClientList = ds.Tables[0].DataSetToEntities<Client>().ToList();

               

                res.Code = 200;
                res.Data = ClientList;
                res.Message = "";
                string Req = JsonConvert.SerializeObject("");
                string Res = JsonConvert.SerializeObject(res);
                string errorLogged = Error.ReqRes("Feedback", "GetAllClient", Req, Res);
                return Json(res);

            }
            catch (Exception ex)
            {

                res.Code = 500;
                res.Data = "";
                res.Message = ex.Message;
                int errorLogged = Error.ErrorLogs("Feedback", "GetAllClient", ex.Message);
                return Json(res);

            }
        }


        [HttpPost]
        public IHttpActionResult GetAllFeedback(FeedbckList IN)
        {
            res.Code = 401;
            res.Data = "";
            res.Message = "Unable to save data";
            try
            {
                int totalCount = 0;

                DataSet ds = Feedbacks.GetAllFeedback(IN,out totalCount);
                List<Feedback> feedbacks = new List<Feedback>();
                List<QuesAns> quesAnss = new List<QuesAns>();
                if (ds.Tables[0].Rows.Count > 0)
                    feedbacks = ds.Tables[0].DataSetToEntities<Feedback>().ToList();
               if (ds.Tables[1].Rows.Count > 0)
                    quesAnss = ds.Tables[1].DataSetToEntities<QuesAns>().ToList();
                foreach (var item in feedbacks)
                {
                    item.QueAns = new List<QuesAns>();
                    item.QueAns = quesAnss.Where(f => f.ClientID == item.ClientID).ToList();
                }

                res.Code = 200;
                res.Data = feedbacks;
                res.Message = "";
                res.totalcount = totalCount;
                string Req = JsonConvert.SerializeObject(IN);
                string Res = JsonConvert.SerializeObject(res);
                string errorLogged = Error.ReqRes("Feedback", "GetAllFeedback", Req, Res);
                return Json(res);

            }
            catch (Exception ex)
            {

                res.Code = 500;
                res.Data = "";
                res.Message = ex.Message;
                int errorLogged = Error.ErrorLogs("Feedback", "GetAllFeedback", ex.Message);
                return Json(res);

            }
        }



        [NonAction]
        public void SendEnquiryMailToUser(string Name, string email)
        {
            MailMessage mail = new MailMessage();
            try
            {
                mail.To.Add(email); // where will send
                mail.From = new MailAddress("Community@postboxcommunications.com", "PostboxCommunications");
                mail.Subject = "Feedback Mail postboxcommunications";
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                // mail body                                
                mail.Body = EnquiryMailToUser(Name);
                // send email, dont change //
                SmtpClient client = new SmtpClient();
                string Email = Convert.ToString(ConfigurationManager.AppSettings["smtpEmail"]);
                string Password = Convert.ToString(ConfigurationManager.AppSettings["smtpPassword"]);
                string Host = Convert.ToString(ConfigurationManager.AppSettings["smtpHost"]);
                int Port = Convert.ToInt16(Convert.ToString(ConfigurationManager.AppSettings["smtpPort"]));
                client.Credentials = new System.Net.NetworkCredential(Email, Password); // set 1 email of gmail and password
                client.Port = Port; // gmail use this port
                client.Host = Host; //define the server that will send email
                string EnableSSL = Convert.ToString(ConfigurationManager.AppSettings["smtpEnableSSL"]);
                client.EnableSsl = Convert.ToBoolean(EnableSSL);
                if (!String.IsNullOrEmpty(mail.Body))
                {
                    client.Send(mail);
                }
                mail.Dispose();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //bool errorLogged = ErrorLog("SendEnquiryMailToUser", "Utility", error, "Email ID : " + Convert.ToString(email));
            }
        }

        [NonAction]
        public string EnquiryMailToUser(string userName)
        {
            try
            {
                string body = string.Empty;
                string EmailImageBaseUrl = ConfigurationManager.AppSettings["EmailBaseUrl"];
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Emailer/UserMailer.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Name}", userName);
                body = body.Replace("{Url}", EmailImageBaseUrl);
                return body;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //bool errorLogged = ErrorLog("ActivationLinkEmailBody", "Utility", error, "Email ID : " + Convert.ToString(userName));
                return null;
            }
        }

        [NonAction]
        public void SendEnquiryMailToHelpdesk(string Name, string email, string Designation, string ClientName)
        {
            MailMessage mail = new MailMessage();
            try
            {
                mail.To.Add(Convert.ToString(ConfigurationManager.AppSettings["HelpdeskEmail"])); // where will send
                mail.From = new MailAddress("Community@postboxcommunications.com", "PostboxCommunications");
                mail.Subject = "Feedback Mail For PostboxCommunications";
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                // mail body                                
                mail.Body = EnquiryMailToHelpdesk(Name, email, Designation, ClientName);
                // send email, dont change //
                SmtpClient client = new SmtpClient();
                string Email = Convert.ToString(ConfigurationManager.AppSettings["smtpEmail"]);
                string Password = Convert.ToString(ConfigurationManager.AppSettings["smtpPassword"]);
                string Host = Convert.ToString(ConfigurationManager.AppSettings["smtpHost"]);
                int Port = Convert.ToInt16(Convert.ToString(ConfigurationManager.AppSettings["smtpPort"]));
                client.Credentials = new System.Net.NetworkCredential(Email, Password); // set 1 email of gmail and password
                client.Port = Port; // gmail use this port
                client.Host = Host; //define the server that will send email
                string EnableSSL = Convert.ToString(ConfigurationManager.AppSettings["smtpEnableSSL"]);
                client.EnableSsl = Convert.ToBoolean(EnableSSL);
                if (!String.IsNullOrEmpty(mail.Body))
                {
                    client.Send(mail);
                }
                mail.Dispose();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //bool errorLogged = ErrorLog("SendEnquiryMailToHelpdesk", "Utility", error, "Email ID : " + Convert.ToString(email));
            }
        }

        [NonAction]
        public string EnquiryMailToHelpdesk(string Name, string email, string Designation, string ClientName)
        {
            try
            {
                string body = string.Empty;
                string EmailImageBaseUrl = ConfigurationManager.AppSettings["EmailBaseUrl"];
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Emailer/index.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Name}", Name);
                body = body.Replace("{Email}", email);
                body = body.Replace("{Designation}", Designation);
                body = body.Replace("{ClientName}", ClientName);
                body = body.Replace("{Url}", EmailImageBaseUrl);
                return body;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //bool errorLogged = ErrorLog("ActivationLinkEmailBody", "Utility", error, "Email ID : " + Convert.ToString(userName));
                return null;
            }
        }

    }

    public static class AdoExtensions
    {
        public static List<T> DataSetToEntities<T>(this DataTable source) where T : new()
        {
            var result = new List<T>();
            Type resultantEntityType = typeof(T);
            if (source.Rows.Count > 0)
            {
                foreach (DataRow row in source.Rows)
                {
                    T data = new T();
                    foreach (PropertyInfo entityProperties in resultantEntityType.GetProperties())
                    {
                        // var value = row[entityProperties.Name] == null ? "" : row[entityProperties.Name];
                        if (row.Table.Columns.Contains(entityProperties.Name))
                            entityProperties.SetValue(data, row[entityProperties.Name], null);
                    }
                    result.Add(data);
                }
            }
            return result;
        }
    }
}