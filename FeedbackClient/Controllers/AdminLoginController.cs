﻿using ClientFeedback.DAL;
using ClientFeedback.Models;
using FeedbackClient.DAL;
using FeedbackClient.Method;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using static FeedbackClient.Models.IPCMSLogin;

namespace FeedbackClient.Controllers
{
    public class AdminLoginController : ApiController
    {

        ErrorLog Error = new ErrorLog();
        [HttpPost]
        public IHttpActionResult AdminLogin(IPEncryptData CS)
        {
            Response res = new Response();
            res.Code = 401;
            res.Data = "";
            res.Message = "Invalid UserId and Password";

            IPLoginDAL logindal = new IPLoginDAL();
            CommonMethod comMethod = new CommonMethod();
            //    CommonDAl commonDAL = new CommonDAl();
            //string projectid = ConfigurationManager.AppSettings["ProjectId"];
            //UserDetail UserDetail = new UserDetail();
            //EmpList EMPModule = new EmpList();
            try
            {
                var secretkey = logindal.GetSecretKey();

                string decryptedstring = Cipher.DecryptStringAES(CS.FormData, secretkey, secretkey);

                IPLogin login = JsonConvert.DeserializeObject<IPLogin>(decryptedstring);

                int executionresult = 0;
                var userdata = logindal.ValidateUser(login, out executionresult);

                if (executionresult == -1)
                {
                    return Json(res);
                }
                else if (executionresult == -2)
                {
                    res.Message = "Invalid Password";
                    return Json(res);
                }
                else if (executionresult > 0)
                {

                    var UserID = userdata.UserId;
                    TokenDetail session = new TokenDetail();

                    IPToken ipToken = new IPToken();
                    ipToken.ExpireTime = Convert.ToInt32(ConfigurationManager.AppSettings["AuthTokenExpiry"]);

                    ipToken.Token = (comMethod.GetSalt() + comMethod.GetSalt() + comMethod.GetSalt()).Replace("+", "").Replace("=", "").Replace("/", "").ToString();
                    ipToken.UserId = UserID;

                    var tokenResult = logindal.SaveToken(ipToken);

                    if (tokenResult > 0)
                    {
                        userdata.Token = ipToken.Token;
                    }

                    var usermodule = logindal.UserModule(UserID);

                    userdata.UserModule = new List<UserModuleMapping>();
                    userdata.UserModule = usermodule;

                    res.Code = 200;
                    res.Error = "";
                    res.Data = userdata;
                    res.Message = "Login Successfully....";
                    string request = JsonConvert.SerializeObject(CS);
                    string response = JsonConvert.SerializeObject(res);
                    string errorLogged = Error.ReqRes("Account", "AdminLogin", request, response);
                    return Json(res);

                }
                else
                {
                    res.Code = 401;
                    res.Error = "";
                    res.Data = "";
                    res.Message = "LoginID OR Password Is Incorrect";
                    return Json(res);
                }
            }
            catch (Exception ex)
            {
                res.Code = 500;
                res.Data = "";
                res.Message = ex.Message;
                int errorLogged = Error.ErrorLogs("AgentDetail", "InsertToken", ex.Message);
                return Json(res);

            }
        }

    }
}