﻿using ClientFeedback.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FeedbackClient.Models.IPCMSLogin;

namespace FeedbackClient.DAL
{
    public class IPLoginDAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Client_Feedback"].ConnectionString);

        public string GetSecretKey()
        {
            SqlCommand cmd1 = new SqlCommand("GetSecretKey", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter ad1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            ad1.Fill(ds1);
            string key = ds1.Tables[0].Rows[0]["SecretKey"].ToString();

            //con.Close();
            return key;
        }

        public OPUserMaster ValidateUser(IPLogin IN, out int ExecutionResult)
        {
            OPUserMaster userdata = new OPUserMaster();
            SqlCommand cmd = new SqlCommand("ValidateUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Userid", IN.UserID == "" ? null : IN.UserID);
            cmd.Parameters.AddWithValue("@Password", IN.Password == "" ? null : IN.Password);
            cmd.Parameters.Add("@ExecutionResult", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            if (ds.Tables.Count > 0)
                userdata = ds.Tables[0].DataSetToEntities<OPUserMaster>().FirstOrDefault();

            ExecutionResult = Convert.ToInt32(cmd.Parameters["@ExecutionResult"].Value);

            return userdata;
        }



        public int SaveToken(IPToken model)
        {

            SqlCommand cmd = new SqlCommand("SaveToken", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserId", model.UserId);
            cmd.Parameters.AddWithValue("@Token", model.Token);
            cmd.Parameters.AddWithValue("@ExpiredTime", model.ExpireTime);
            cmd.Parameters.Add("@ExecutionResult", SqlDbType.Int).Direction = ParameterDirection.Output;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            int result = Convert.ToInt32(cmd.Parameters["@ExecutionResult"].Value);

            return result;

        }


        public List<UserModuleMapping> UserModule(Guid UserId)
        {
            List<UserModuleMapping> module = new List<UserModuleMapping>();
            SqlCommand cmd = new SqlCommand("[GetUserModule]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Userid", UserId);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            if (ds.Tables.Count > 0)
                module = ds.Tables[0].DataSetToEntities<UserModuleMapping>().ToList();

            return module;
        }




    }
}