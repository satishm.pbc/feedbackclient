﻿using ClientFeedback.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ClientFeedback.DAL
{
    public class Feedbackdal
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Client_Feedback"].ConnectionString);


        public string InsertFeedback(Feedback IN)
        {

            SqlCommand cmd = new SqlCommand("InsertFeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ClientID", IN.ClientID == "" ? null : IN.ClientID);
            cmd.Parameters.AddWithValue("@Name", IN.Name == "" ? null : IN.Name);
            cmd.Parameters.AddWithValue("@EmailID", IN.EmailID == "" ? null : IN.EmailID);
            cmd.Parameters.AddWithValue("@Designation", IN.Designation == "" ? null : IN.Designation);
            cmd.Parameters.AddWithValue("@Testimonial", IN.Testimonial == "" ? null : IN.Testimonial);
            cmd.Parameters.AddWithValue("@Rating", IN.Rating == "" ? null : IN.Rating);
            cmd.Parameters.AddWithValue("@ProfileLink", IN.ProfileLink == "" ? null : IN.ProfileLink);
            cmd.Parameters.Add("@ExecutionResult", SqlDbType.VarChar, (500)).Direction = ParameterDirection.Output;
            // cmd.Parameters["@executionresult"].Direction = ParameterDirection.Output;
            if (IN.QueAns != null)
            {
                DataTable QuesAnswer = new DataTable();
                QuesAnswer.Columns.Add("QuesText", typeof(string));
                QuesAnswer.Columns.Add("AnsTesxt", typeof(string));

                for (int i = 0; i < IN.QueAns.Count; i++)
                {
                    QuesAnswer.Rows.Add(IN.QueAns[i].QuesText, IN.QueAns[i].AnsText);
                }
                cmd.Parameters.AddWithValue("@QuesAns", QuesAnswer);
            }
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            string result = (cmd.Parameters["@ExecutionResult"].Value).ToString();

            //con.Close();
            return result;
        }


        public DataSet GetAllClient()
        {

            SqlCommand cmd = new SqlCommand("GetAllCompanyListddl", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters["@executionresult"].Direction = ParameterDirection.Output;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            //con.Close();
            return ds;
        }

        public DataSet GetAllFeedback(FeedbckList IN,out int totalCount)
        {

            SqlCommand cmd = new SqlCommand("GetAllFeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Start", IN.Start);
            cmd.Parameters.AddWithValue("@Length", IN.Length);
            cmd.Parameters.Add("@totalcount", SqlDbType.Int).Direction = ParameterDirection.Output;
            // cmd.Parameters["@executionresult"].Direction = ParameterDirection.Output;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            totalCount = Convert.ToInt32((cmd.Parameters["@totalcount"].Value).ToString());

            //con.Close();
            return ds;
        }

    }
}