﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ClientFeedback.DAL
{
    public class ErrorLog
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Client_Feedback"].ConnectionString);

        public int ErrorLogs(string controller, string action, string message)
        {

            SqlCommand cmd = new SqlCommand("SaveError", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@controller", controller);
            cmd.Parameters.AddWithValue("@action", action);
            cmd.Parameters.AddWithValue("@message", message);

            cmd.Parameters.Add("@executionresult", SqlDbType.Int).Direction = ParameterDirection.Output;
            //cmd.Parameters["@executionresult"].Direction = ParameterDirection.Output;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            int result = Convert.ToInt32(cmd.Parameters["@executionresult"].Value);

            return result;
        }


        public string ReqRes(string controller, string action, string req, string res)
        {

            SqlCommand cmd = new SqlCommand("SaveReqRess", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControllerName", controller);
            cmd.Parameters.AddWithValue("@ActionName", action);
            cmd.Parameters.AddWithValue("@Req", req);
            cmd.Parameters.AddWithValue("@Res", res);


            cmd.Parameters.Add("@ExecutionResult", SqlDbType.VarChar, (500)).Direction = ParameterDirection.Output;
            //cmd.Parameters["@executionresult"].Direction = ParameterDirection.Output;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            string result = (cmd.Parameters["@ExecutionResult"].Value).ToString();

            return result;
        }

    }
}