﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientFeedback.Models
{
    public class Feedback
    {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string Name { get; set; }
        public string EmailID { get; set; }
        public string Designation { get; set; }
        public string Testimonial { get; set; }
        public string Rating { get; set; }
        public string ProfileLink { get; set; }
        public List<QuesAns> QueAns {get;set;}


    }

public class QuesAns
{
        public string ClientID { get; set; }
        public string QuesText { get; set; }
    public string AnsText { get; set; }

}
    public class FeedbckList
    {
        public int Start { get; set; }
        public int Length { get; set; }

    }

    public class Client
    {
        public string CID { get; set; }
        public string C_Name { get; set; }
    }
}