﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedbackClient.Models
{
    public class IPCMSLogin
    {
        public class IPEncryptData
        {
            public string FormData { get; set; }
        }


        public class IPLogin
        {
            public string UserID { get; set; }
            public string Password { get; set; }
        }


        public class OPUserMaster
        {
            public Guid UserId { get; set; }
            public string Name { get; set; }
            public string EmailId { get; set; }
            public string Mobile { get; set; }
            //public string Password { get; set; }
            public int UserTypeId { get; set; }
            public string Token { get; set; }
            public List<UserModuleMapping> UserModule { get; set; }

        }

        public class AgentCodes
        {

            public string AgentCode { get; set; }
            public string Token { get; set; }

        }

        public class IPToken
        {
            public Guid UserId { get; set; }
            public string Token { get; set; }
            public int ExpireTime { get; set; }
            public string AgentCode { get; set; }
        }

        public class TokenDetail
        {
            public int TokenId { get; set; }
            public int UserId { get; set; }
            public string Token { get; set; }
            public DateTime CreatedOn { get; set; }
            public DateTime ExpiredOn { get; set; }

        }

        public class UserModuleMapping
        {
            public string UserID { get; set; }
            public string ModuleID { get; set; }
            public string Module { get; set; }
            public bool ViewOnly { get; set; }
            public bool Modify { get; set; }
            public string DesignationID { get; internal set; }
        }


    }
}